package queue;

public class ArrayQueueModule {
    private static int head;
    private static int tail;
    private static Object[] elements = new Object[5];

    // Pre:  element != null
    // Post: elements[tail] = element && tail' = (tail+1)%elements.length
    public static void enqueue(Object element) {
        assert element != null;
        ensureCapacity(tail + 1);
        elements[tail] = element;
        tail = (tail + 1) % (elements.length);
    }

    // Pre:
    // Post: capacity < elements'.length &&
    //        forall i=0..elements.length
    //        elements' contains elements[i]
    //        forall i=0..elements.length-1
    //        elements'[i] = elements[j]
    //        elements'[i+1] = elements[j+1]
    private static void ensureCapacity(int capacity) {
        if (capacity < elements.length) {
            return;
        }
        Object[] newElements = copySomeElems(2 * capacity);
        if (tail < head) {
            head = 0;
            tail = size() - head + tail + 1;
        } else {
            tail = size();
            head = 0;
        }
        elements = newElements;
    }

    // Pre: size > 0
    // Post: returned = elements[head] && head' = (head + 1) % elements.length
    //        forall i=0..elements.length
    //        if (i!=head)
    //        elements' = elements[i]
    public static Object dequeue() {
        assert size() != 0;
        Object ans = elements[head];
        head = (head + 1) % elements.length;
        return ans;
    }

    // Pre:  size > 0
    // Post: returned = elements[head]
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static Object element() {
        assert size() != 0;

        return elements[head];
    }

    // Pre:
    // Post: returned = amount of objects in queue
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static int size() {
        if (head <= tail) {
            return Math.abs(tail - head);
        } else {
            return elements.length - Math.abs(tail - head);
        }
    }

    // Pre:
    // Post: returned = amount of objects in queue equals 0
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static boolean isEmpty() {
        return size() == 0;
    }

    // Pre:
    // Post:  head = 0
    //        tail = 0
    //        elements[0] = null
    public static void clear() {
        tail = 0;
        head = 0;
        elements[0] = null;
    }

    // Pre:
    // Post:  forall i=0..elements.length
    //        elements'[i] = elements[i]
    //        returned = array of queue elements
    //        forall i=head..tail-1
    //        array[i-head] = elements[i]
    public static Object[] toArray() {
        return copySomeElems(size());
    }

    private static Object[] copySomeElems(int newSize) {
        Object[] elems = new Object[newSize];
        if (tail < head) {
            System.arraycopy(elements, head, elems, 0, size() - head);
            System.arraycopy(elements, 0, elems, size() - head + 1, tail);
        } else {
            System.arraycopy(elements, head, elems, 0, tail - head);
        }
        return elems;
    }
}