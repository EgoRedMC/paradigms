package queue;

import java.util.Date;

public class ArrayQueueTest {
    public static void main(String[] args) {
        test();
    }

    static void test() {
        Date d = new Date();
        ArrayQueue q = new ArrayQueue();
        fill(q);
        dump(q);
        System.out.printf("работало %d мсекунд", new Date().getTime() - d.getTime());
    }

    static void fill(ArrayQueue q) {
        for (int i = 0; i < 100000; i++) {
            q.enqueue(i);
        }
    }

    static void dump(ArrayQueue q) {
        while (!q.isEmpty()) {
            System.out.println(q.size() + " " + q.element() + " " + q.dequeue());
        }
    }
}

