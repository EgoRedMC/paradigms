package queue;

public class ArrayQueueADT {
    private int head;
    private int tail;
    private Object[] elements = new Object[5];

    // Pre:  element != null && queue != null
    // Post: elements[tail] = element && tail' = (tail+1)%elements.length
    public static void enqueue(ArrayQueueADT queue, Object element) {
        assert element != null;
        ensureCapacity(queue, queue.tail + 1);
        queue.elements[queue.tail] = element;
        queue.tail = (queue.tail + 1) % (queue.elements.length);
    }

    // Pre:  queue != null
    // Post: capacity < elements'.length &&
    //        forall i=0..elements.length
    //        elements' contains elements[i]
    //        forall i=0..elements.length-1
    //        elements'[i] = elements[j]
    //        elements'[i+1] = elements[j+1]
    private static void ensureCapacity(ArrayQueueADT queue, int capacity) {
        if (capacity < queue.elements.length) {
            return;
        }
        Object[] newElements = copySomeElems(queue, 2 * capacity);
        if (queue.tail < queue.head) {
            queue.head = 0;
            queue.tail = size(queue) - queue.head + queue.tail + 1;
        } else {
            queue.tail = size(queue);
            queue.head = 0;
        }
        queue.elements = newElements;
    }

    // Pre: size > 0 && queue != null
    // Post: returned = elements[head] && head' = (head + 1) % elements.length
    //        forall i=0..elements.length
    //        if (i!=head)
    //        elements' = elements[i]
    public static Object dequeue(ArrayQueueADT queue) {
        assert size(queue) != 0;
        Object ans = queue.elements[queue.head];
        queue.head = (queue.head + 1) % queue.elements.length;
        return ans;
    }

    // Pre:  size > 0 && queue != null
    // Post: returned = elements[head]
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static Object element(ArrayQueueADT queue) {
        assert size(queue) != 0;

        return queue.elements[queue.head];
    }

    // Pre:  queue != null
    // Post: returned = amount of objects in queue
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static int size(ArrayQueueADT queue) {
        if (queue.head <= queue.tail) {
            return Math.abs(queue.tail - queue.head);
        } else {
            return queue.elements.length - Math.abs(queue.tail - queue.head);
        }
    }

    // Pre:  queue != null
    // Post: returned = amount of objects in queue equals 0
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public static boolean isEmpty(ArrayQueueADT queue) {
        return size(queue) == 0;
    }

    // Pre:   queue != null
    // Post:  head = 0
    //        tail = 0
    //        elements[0] = null
    public static void clear(ArrayQueueADT queue) {
        queue.tail = 0;
        queue.head = 0;
        queue.elements[0] = null;
    }

    // Pre:   queue != null
    // Post:  forall i=0..elements.length
    //        elements'[i] = elements[i]
    //        returned = array of queue elements
    //        forall i=head..tail-1
    //        array[i-head] = elements[i]
    public static Object[] toArray(ArrayQueueADT queue) {
        return copySomeElems(queue, size(queue));
    }

    private static Object[] copySomeElems(ArrayQueueADT queue, int newSize) {
        Object[] elems = new Object[newSize];
        if (queue.tail < queue.head) {
            System.arraycopy(queue.elements, queue.head, elems, 0, size(queue) - queue.head);
            System.arraycopy(queue.elements, 0, elems, size(queue) - queue.head + 1, queue.tail);
        } else {
            System.arraycopy(queue.elements, queue.head, elems, 0, queue.tail - queue.head);
        }
        return elems;
    }
}
