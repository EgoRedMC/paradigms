package queue;

public interface Queue {

    // Pre:  element != null  && this != null
    // Post: elements[tail] = element && tail' = (tail+1)%elements.length
    public void enqueue(Object o);

    // Pre: size > 0  && this != null
    // Post: returned = elements[head] && head' = (head + 1) % elements.length
    //        forall i=0..elements.length
    //        if (i!=head)
    //        elements' = elements[i]
    public Object dequeue();

    // Pre:  size > 0 && this != null
    // Post: returned = elements[head]
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public Object element();

    // Pre:  this != null
    // Post: returned = amount of objects in queue
    //        forall i=0..elements.length
    //        elements'[i] = elements[i]
    public int size();

    // Pre:  this != null
    // Post: returned = amount of objects in queue equals 0
    //        elements'[i] = elements[i]
    //        forall i=0..elements.length
    public boolean isEmpty();

    // Pre:   this != null
    // Post:  head = 0
    //        tail = 0
    //        elements[0] = null
    public void clear();

    // Pre:   this != null
    // Post:  forall i=0..elements.length
    //        elements'[i] = elements[i]
    //        returned = array of queue elements
    //        forall i=head..tail-1
    //        array[i-head] = elements[i]
    public Object[] toArray();

}
