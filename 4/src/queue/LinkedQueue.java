package queue;

public class LinkedQueue extends AbstractQueue {

    private class Node {
        Object data;
        Node next;

        private Node(Object o) {
            data = o;
        }
    }

    private Node first;
    private Node last;

    @Override
    public void enqueueImpl(Object o) {
        Node n = new Node(o);
        if (size == 0) {
            first = n;
            last = n;
        } else {
            last.next = n;
            last = last.next;
        }
    }

    @Override
    public void dequeueImpl() {
        first = first.next;
    }

    @Override
    public Object elementImpl() {
        return first.data;
    }

    @Override
    public void clearImpl() {
        first = null;
        last = null;
    }


}
