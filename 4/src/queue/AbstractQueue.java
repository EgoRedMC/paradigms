package queue;

public abstract class AbstractQueue implements Queue {
    protected int size = 0;

    @Override
    public void enqueue(Object element) {
        assert element != null;
        enqueueImpl(element);
        size++;
    }

    protected abstract void dequeueImpl();

    //
    @Override
    public Object dequeue() {
        Object res = element();
        size--;
        dequeueImpl();
        return res;
    }

    protected abstract void enqueueImpl(Object element);

    //
    @Override
    public void clear() {
        size = 0;
        clearImpl();
    }

    protected abstract void clearImpl();

    @Override
    public Object element() {
        assert size > 0;
        return elementImpl();
    }

    protected abstract Object elementImpl();

    //
    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public Object[] toArray() {
        Object[] elems = new Object[size];
        int s = size;
        for (int i = 0; i < s; i++) {
            elems[i] = this.dequeue();
            this.enqueue(elems[i]);
        }
        return elems;
    }
}