package queue;

public class ArrayQueue extends AbstractQueue {
    private int head = 0;
    private int tail = 0;
    private Object[] elements = new Object[5];


    public void enqueueImpl(Object element) {
        ensureCapacity(tail + 1);
        elements[tail] = element;
        tail = (tail + 1) % (elements.length);
    }

    private void ensureCapacity(int capacity) {
        if (capacity < elements.length) {
            return;
        }
        Object[] newElements = new Object[2 * capacity];
        if (tail < head) {
            System.arraycopy(elements, head, newElements, 0, size() - head);
            System.arraycopy(elements, 0, newElements, size() - head + 1, tail);
            head = 0;
            tail = size() - head + tail + 1;
        } else {
            System.arraycopy(elements, head, newElements, 0, tail - head);
            tail = size();
            head = 0;
        }
        elements = newElements;
    }

    public void dequeueImpl() {
        assert size() != 0;
        head = (head + 1) % elements.length;
    }


    public Object elementImpl() {
        return elements[head];
    }


    public void clearImpl() {
        tail = 0;
        head = 0;
        elements[head] = null;
    }


}
