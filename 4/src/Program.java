import queue.LinkedQueue;

import java.util.Arrays;

public class Program {
    public static void main(String[] args) {
        LinkedQueue lq = new LinkedQueue();
        lq.enqueue(1);
        lq.enqueue(3);
        lq.enqueue(5);
        System.out.println(lq.size());
        System.out.println(lq.element());
        System.out.println(lq.dequeue());
        System.out.println(Arrays.toString(lq.toArray()));
        lq.clear();
        System.out.println(Arrays.toString(lq.toArray()));
        System.out.println(lq.isEmpty());
    }
}
