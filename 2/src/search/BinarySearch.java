package search;

public class BinarySearch {
    //args.length >= 2
    //args[0] - key для поиска
    // forall i=1..args.length - 2:
    //args[i] >= args[i+1]
    public static void main(String[] args) {

        //args[0] - key для поиска
        int x = Integer.parseInt(args[0]);
        //x - key для поиска

        // forall i=1..args.length - 2:
        //args[i] >= args[i+1]
        int[] arr = new int[args.length - 1];
        for (int i = 0; i < args.length - 1; i++) {
            arr[i] = Integer.parseInt(args[i + 1]);
        }
        // forall i=0..arr.length - 2:
        //arr[i] >= arr[i+1]

        System.out.println(binSearchRec(x, arr));
        //return: min(i) -> args[i+1] <= args[0]
    }
    //return: min(i) -> args[i+1] <= args[0]


    //x - key для поиска
    //arr.length >= 1
    // forall i=0..arr.length - 2:
    //arr[i] >= arr[i+1]
    // (i = min(i) -> arr[i] <= x) && l < i && i < r
    private static int binSearchIt(int x, int[] arr) {

        int l = -1, r = arr.length;
        // l < 0 && r > arr.length - 1
        // r > l + 1

        // inv: r > l + 1
        // inv: arr[l] > x && arr[r] <= x
        while (l < r - 1) {

            // inv
            int m = (l + r) / 2;
            // r > m && m > l

            // r > m && m > l
            // 0 <= m && m <= arr.length - 1
            if (arr[m] <= x)
                // forall i=m..arr.length - 1:
                //arr[i] <= x

                //arr[m] <= x
                r = m;
                //arr[r] <= x
            else
                // forall i=0..m :
                //arr[i] > x

                //arr[m] > x
                l = m;
                //arr[l] > x
        }
        // l == r + 1 &&   arr[l] > x && arr[r] <= x

        return r;
        //return: min(i) -> arr[i] <= x
    }
    //return: min(i) -> arr[i] <= x
    // forall i=0..arr.length - 1:
    // arr[i] == arr'[i]

    //x - key для поиска
    //arr.length >= 1
    // forall i=0..arr.length - 2:
    //arr[i] >= arr[i+1]
    // (i = min(i) -> arr[i] <= x) && l < i && i <= r
    private static int binSearchRec(int x, int[] arr) {
        //x - key для поиска
        //arr.length >= 1
        // forall i=0..arr.length - 2:
        //arr[i] >= arr[i+1]
        // l + 1 < r
        return search(x, arr, -1, arr.length);
        //return: min(i) -> arr[i] <= x

    }
    //return: min(i) -> arr[i] <= x
    // forall i=0..arr.length - 1:
    // arr[i] == arr'[i]

    //x - key для поиска
    //arr.length >= 1
    // forall i=0..arr.length - 2:
    //arr[i] >= arr[i+1]
    // (i = min(i) -> arr[i] <= x) && l < i && i <= r
    //(l == r - 1 && arr[r] <= x && arr[l] > x) || (l < r - 1 && ((forall i=(l+r)/2..arr.length-1    arr[i]<=x) || (forall i=0..(l+r)/2-1  arr[i]>x)))
    private static int search(int x, int[] arr, int l, int r) {

        //(l == r - 1 && arr[r] <= x && arr[l] > x) || (l < r - 1 && ((forall i=(l+r)/2..arr.length-1    arr[i]<=x) || (forall i=0..(l+r)/2-1  arr[i]>x)))
        return l < r - 1 ? arr[(l + r) / 2] <= x ? search(x, arr, l, (l + r) / 2) : search(x, arr, (l + r) / 2, r) : r;
        //return: min(i) -> arr[i] <= x

    }
    //return: min(i) -> arr[i] <= x
    // forall i=0..arr.length - 1:
    // arr[i] == arr'[i]
}
