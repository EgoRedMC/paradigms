import expression.operations.*;
import expression.variables.*;

public class Main {
    public static void main(String[] args) {
        System.out.println(new Subtract(
                new Multiply(
                        new Const(2.1),
                        new Variable("x")
                ),
                new Const(3.2)
        ).evaluate(5.0));
    }
}
