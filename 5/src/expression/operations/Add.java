package expression.operations;

import com.sun.xml.internal.bind.annotation.OverrideAnnotationOf;
import expression.Expression;
import expression.NumberExpression;

public class Add extends AbstractExpression {
    public Add(NumberExpression e1, NumberExpression e2) {
        super(e1, e2);
    }

    @Override
    public int operation(int a, int b) {
        return a + b;
    }

    @Override
    public double operation(double a, double b) {
        return a + b;
    }
}
