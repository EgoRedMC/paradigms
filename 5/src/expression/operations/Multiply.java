package expression.operations;

import expression.Expression;
import expression.NumberExpression;

public class Multiply extends AbstractExpression {

    public Multiply(NumberExpression e1, NumberExpression e2) {
        super(e1, e2);
    }

    @Override
    public int operation(int a, int b) {
        return a * b;
    }

    @Override
    public double operation(double a, double b) {
        return a * b;
    }
}
