package expression.operations;

import expression.DoubleExpression;
import expression.Expression;
import expression.NumberExpression;

abstract class AbstractExpression implements NumberExpression {
    NumberExpression e1, e2;

    AbstractExpression(NumberExpression e1, NumberExpression e2) {
        assert e1 != null;
        assert e2 != null;
        this.e1 = e1;
        this.e2 = e2;
    }

    @Override
    public int evaluate(int x) {
        return operation(this.e1.evaluate(x), this.e2.evaluate(x));
    }

    @Override
    public double evaluate(double x) {
        return operation(this.e1.evaluate(x), this.e2.evaluate(x));

    }

    abstract int operation(int a, int b);

    abstract double operation(double a, double b);
}
