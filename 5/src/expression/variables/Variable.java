package expression.variables;

import expression.DoubleExpression;
import expression.Expression;
import expression.NumberExpression;

public class Variable implements NumberExpression {
    private String name;
    double value;

    public Variable(String name) {
        this.name = name;
    }

    @Override
    public int evaluate(int x) {
        value = x;
        return (int) value;
    }

    @Override
    public double evaluate(double x) {
        this.value = x;
        return value;
    }
}
