package expression.variables;

public class Const extends Variable {

    public Const(int x) {
        super("");
        this.value = x;
    }

    public Const(double d) {
        super("");
        this.value = d;
    }

    @Override
    public int evaluate(int x) {
        return (int) value;
    }

    @Override
    public double evaluate(double x) {
        return value;
    }
}
