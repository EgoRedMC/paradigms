package expression;

import expression.DoubleExpression;
import expression.Expression;

public interface NumberExpression extends Expression, DoubleExpression {
    @Override
    double evaluate(double x);

    @Override
    int evaluate(int x);
}
