import java.io.*;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CalcMD5Experiments {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        for (int i = 0; i < Files.readAllLines(new File(args[0]).toPath()).size(); i++) {
            System.out.println(new BigInteger(1, MessageDigest.getInstance("MD5").digest(Files.readAllBytes(new File(Files.readAllLines(new File(args[0]).toPath()).get(i)).toPath()))).toString(16));
        }
    }
}

/*
public class CalcMD5Experiments {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        for (int i = 0; i < Files.readAllLines(new File(args[0]).toPath()).size(); i++) {
            byte[] t = MessageDigest.getInstance("MD5").digest(Files.readAllBytes(new File(Files.readAllLines(new File(args[0]).toPath()).get(i)).toPath()));
            //List<Byte> l = Arrays.asList(t);
            //List<String> res = l.stream().map((Byte x) -> String.format("%02x", x)).collect(Collectors.toList());
        }
    }
}

*/
