import java.io.*;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SHA256Sum {
    public static void main(String[] args) {
        try {
            if (args.length == 0) {
                byte[] buffer = new byte[System.in.available()];
                System.in.read(buffer);
                byte[] bytes = MessageDigest.getInstance("SHA-256").digest(buffer);
                for (byte aByte : bytes) {
                    System.out.printf("%02x", aByte);
                }
                System.out.println(" *-");
            } else {
                for (String arg : args) {
                    for (byte b : MessageDigest.getInstance("SHA-256").digest(Files.readAllBytes(new File(arg).toPath()))) {
                        System.out.format("%02x", b);
                    }
                    System.out.printf(" *%s\n", arg);
                }
            }
        } catch (IOException e) {
            System.out.println("Wrong filename/filenames");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Wrong Algorithm");
        }
    }

}
