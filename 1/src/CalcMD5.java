import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CalcMD5 {
    public static void main(String[] args) throws IOException, NoSuchAlgorithmException {
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(args[0]), StandardCharsets.UTF_8));
        MessageDigest md = MessageDigest.getInstance("MD5");
        while (reader.ready()) {
            for (byte b : md.digest(Files.readAllBytes(new File(reader.readLine()).toPath()))) {
                System.out.format("%02x", b);
            }
            System.out.println();
        }
        reader.close();
    }
}

